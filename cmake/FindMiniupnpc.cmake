# Copyright (c) 2015-2019, The Kovri I2P Router Project
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are
# permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of
#    conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list
#    of conditions and the following disclaimer in the documentation and/or other
#    materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be
#    used to endorse or promote products derived from this software without specific
#    prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
# THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
# THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Parts of this file are originally copyright (c) 2014-2018 The Monero Project

#
# Locate miniupnp library
# This module defines
#  MINIUPNP_FOUND, if false, do not try to link to miniupnp
#  MINIUPNP_LIBRARY, the miniupnp variant
#  MINIUPNP_INCLUDE_DIR, where to find miniupnpc.h and family)
#  MINIUPNPC_VERSION_1_7_OR_HIGHER, set if we detect the version of miniupnpc is 1.7 or higher
#
# NOTE: If you wish to use a non-system-default miniupnp installation location, pass
#
#     MINIUPNP_INCLUDE_DIR=<path to miniupnpc include>
#     MINIUPNP_LIBRARY=<path to miniupnpc lib>
#
#  directly to `make` or export into your shell environment. Otherwise, this should find your install.
#

find_path(MINIUPNP_INCLUDE_DIR miniupnpc.h
  HINTS $ENV{MINIUPNP_INCLUDE_DIR}
  PATH_SUFFIXES miniupnpc)

set(MINIUPNP_SHORT_LIB libminiupnpc.a)
if (NOT WITH_STATIC)
  set(MINIUPNP_SHORT_LIB miniupnpc)
endif()

find_library(MINIUPNP_LIBRARY ${MINIUPNP_SHORT_LIB}
  HINTS $ENV{MINIUPNP_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  MiniUPnPc DEFAULT_MSG
  MINIUPNP_INCLUDE_DIR
  MINIUPNP_LIBRARY)

# TODO(unassigned): outdated, higher requisite to match API changes
if (MINIUPNPC_FOUND)
  file(STRINGS "${MINIUPNP_INCLUDE_DIR}/miniupnpc.h" MINIUPNPC_API_VERSION_STR REGEX "^#define[\t ]+MINIUPNPC_API_VERSION[\t ]+[0-9]+")
  if (MINIUPNPC_API_VERSION_STR MATCHES "^#define[\t ]+MINIUPNPC_API_VERSION[\t ]+([0-9]+)")
    set(MINIUPNPC_API_VERSION "${CMAKE_MATCH_1}")
    if (${MINIUPNPC_API_VERSION} GREATER "10" OR ${MINIUPNPC_API_VERSION} EQUAL "10")
      message(STATUS "Found miniupnpc API version " ${MINIUPNPC_API_VERSION})
      set(MINIUPNP_FOUND true)
      set(MINIUPNPC_VERSION_1_7_OR_HIGHER true)
    endif()
  endif()
endif()
mark_as_advanced(MINIUPNP_INCLUDE_DIR MINIUPNP_LIBRARY)
