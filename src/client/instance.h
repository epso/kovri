/**                                                                                           //
 * Copyright (c) 2015-2018, The Kovri I2P Router Project                                      //
 *                                                                                            //
 * All rights reserved.                                                                       //
 *                                                                                            //
 * Redistribution and use in source and binary forms, with or without modification, are       //
 * permitted provided that the following conditions are met:                                  //
 *                                                                                            //
 * 1. Redistributions of source code must retain the above copyright notice, this list of     //
 *    conditions and the following disclaimer.                                                //
 *                                                                                            //
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list     //
 *    of conditions and the following disclaimer in the documentation and/or other            //
 *    materials provided with the distribution.                                               //
 *                                                                                            //
 * 3. Neither the name of the copyright holder nor the names of its contributors may be       //
 *    used to endorse or promote products derived from this software without specific         //
 *    prior written permission.                                                               //
 *                                                                                            //
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY        //
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF    //
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL     //
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       //
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,               //
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    //
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,          //
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF    //
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               //
 */

#ifndef SRC_CLIENT_INSTANCE_H_
#define SRC_CLIENT_INSTANCE_H_

#include <memory>
#include <string>
#include <vector>

#include "client/tunnel_data.h"
#include "core/instance.h"

namespace kovri
{
namespace client
{
// TODO(unassigned): properly remove the need to forward declare for external API usage
class Configuration;

/// @notes Not to be confused with client TunnelData message
class TunnelData
{
 public:
  explicit TunnelData(const TunnelAttributes& tunnel);
  virtual ~TunnelData() = default;

  TunnelData(const TunnelData&) = default;
  TunnelData& operator=(const TunnelData&) = default;

  TunnelData(TunnelData&&) = default;
  TunnelData& operator=(TunnelData&&) = default;

 protected:
  TunnelAttributes m_Tunnel;
  bool m_IsClient{false}, m_IsServer{false};
};

/// @notes Because of context singletons, there is no need to hook-in implementations
/// @todo Finish
class TunnelStatus final : private TunnelData
{
 public:
  explicit TunnelStatus(const TunnelAttributes& tunnel) : TunnelData(tunnel) {}

 public:
  bool IsRunning() const;
  std::string GetState() const;
  std::uint64_t GetUptime() const;
};

/// @notes Because of context singletons, there is no need to hook-in implementations
class Tunnel final : private TunnelData
{
 public:
  explicit Tunnel(const TunnelAttributes& tunnel) : TunnelData(tunnel) {}

 public:
  std::pair<bool, std::string> Configure();
  std::pair<bool, std::string> Start();
  std::pair<bool, std::string> Restart();
  std::pair<bool, std::string> Stop();
};

/// @class Instance
/// @brief Client instance implementation
/// @note Owns core instance object
/// @note It is currently implied that only a single configuration object will
///   be used by a single instance object.
class Instance
{
 public:
  explicit Instance(const core::Instance& core);
  ~Instance() = default;

  Instance(const Instance&) = default;
  Instance& operator=(const Instance&) = default;

  Instance(Instance&&) = default;
  Instance& operator=(Instance&&) = default;

 public:
  /// @brief Initializes the router's client context object
  /// @details Creates tunnels, proxies and I2PControl service
  void Initialize();

  /// @brief Starts instance
  void Start();

  /// @brief Stops instance
  void Stop();

  /// @brief Reloads configuration
  /// @notes TODO(unassigned): should also reload client context
  void Reload();

  /// @brief Get client configuration object
  /// @return Reference to configuration object
  const Configuration& GetConfig() const noexcept
  {
    return *m_Config;
  }

  /// @brief Core lib (router) status accessor
  /// @notes Currently only a core lib status accessor
  const std::shared_ptr<core::Instance::Status>& GetStatus() const
  {
    return m_Core->GetStatus();
  }

  /// @brief Entry point to a client tunnel
  /// @param tunnel Tunnel's attributes for tunnel creation
  /// @return Copy of initialized pointer to shared tunnel object
  std::shared_ptr<Tunnel> ClientTunnel(const TunnelAttributes& tunnel)
  {
    // TODO(unassigned): we don't *need* to throw (this should be done better)
    if (m_IsReloading)
      throw std::runtime_error("Client is reloading, try again later");
    return std::make_shared<Tunnel>(tunnel);
  }

  /// @brief Entry point to a client tunnel status
  /// @param tunnel Tunnel's attributes for tunnel creation
  /// @return Copy of initialized pointer to shared tunnel status object
  std::shared_ptr<TunnelStatus> ClientTunnelStatus(
      const TunnelAttributes& tunnel)
  {
    // TODO(unassigned): we don't *need* to throw (this should be done better)
    if (m_IsReloading)
      throw std::runtime_error("Client is reloading, try again later");
    return std::make_shared<TunnelStatus>(tunnel);
  }

 private:
  /// @brief Sets up (or reloads) client/server tunnels
  /// @warning Configuration files must be parsed prior to setup
  void SetupTunnels();

 private:
  /// @brief Exception dispatcher
  static core::Exception m_Exception;

  /// @brief Core instance (router)
  std::shared_ptr<core::Instance> m_Core;

  /// @brief Client configuration implementation
  /// @note Must be initialized with core configuration
  std::shared_ptr<Configuration> m_Config;

  /// @brief Is client configuration in the process of reloading?
  /// TODO(unassigned): expand types of reloading
  bool m_IsReloading;
};

}  // namespace client
}  // namespace kovri

#endif  // SRC_CLIENT_INSTANCE_H_
