/**                                                                                           //
 * Copyright (c) 2015-2019, The Kovri I2P Router Project                                      //
 *                                                                                            //
 * All rights reserved.                                                                       //
 *                                                                                            //
 * Redistribution and use in source and binary forms, with or without modification, are       //
 * permitted provided that the following conditions are met:                                  //
 *                                                                                            //
 * 1. Redistributions of source code must retain the above copyright notice, this list of     //
 *    conditions and the following disclaimer.                                                //
 *                                                                                            //
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list     //
 *    of conditions and the following disclaimer in the documentation and/or other            //
 *    materials provided with the distribution.                                               //
 *                                                                                            //
 * 3. Neither the name of the copyright holder nor the names of its contributors may be       //
 *    used to endorse or promote products derived from this software without specific         //
 *    prior written permission.                                                               //
 *                                                                                            //
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY        //
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF    //
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL     //
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       //
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,               //
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    //
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,          //
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF    //
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               //
 *                                                                                            //
 * Parts of the project are originally copyright (c) 2013-2015 The PurpleI2P Project          //
 */

#include "client/context.h"

#include <fstream>
#include <iostream>
#include <memory>
#include <mutex>
#include <set>
#include <vector>

#include "core/router/identity.h"
#include "core/util/filesystem.h"
#include "core/util/log.h"

namespace kovri
{
namespace client
{
template <typename t_destination>
std::shared_ptr<t_destination> LocalDestinations<t_destination>::Create(
    const core::PrivateKeys& keys,
    const bool is_public,
    const std::map<std::string, std::string>* params)
{
  std::shared_ptr<t_destination> dest;
  const core::IdentHash& ident = keys.GetPublic().GetIdentHash();
  // Create if doesn't exist
  dest = Find(ident);
  if (dest)
    {
      LOG(debug) << "LocalDestinations: " << ident.ToBase32() << " exists";
    }
  else
    {
      dest = std::make_shared<t_destination>(keys, is_public, params);
      std::unique_lock<std::mutex> lock(m_Mutex);
      m_Destinations[dest->GetIdentHash()] = dest;
      lock.unlock();
    }
  // Automatically start for the sake of fake RAII (see all the other TODOs)
  if (!dest->IsRunning())
    dest->Start();
  return dest;
}

template <typename t_destination>
std::shared_ptr<t_destination> LocalDestinations<t_destination>::LoadFromFile(
    const std::string& filename,
    bool is_public)
{
  core::PrivateKeys keys;
  keys.LoadFromFile(filename);
  return Create(keys, is_public);
}

template <typename t_destination>
std::shared_ptr<t_destination> LocalDestinations<t_destination>::Find(
    std::shared_ptr<t_destination> dest)
{
  if (!dest)
    return nullptr;
  return Find(dest->GetIdentHash());
}

template <typename t_destination>
std::shared_ptr<t_destination> LocalDestinations<t_destination>::Find(
    const core::IdentHash& ident)
{
  std::lock_guard<std::mutex> lock(m_Mutex);
  auto it = m_Destinations.find(ident);
  return it != m_Destinations.end() ? it->second : nullptr;
}

template <typename t_destination>
bool LocalDestinations<t_destination>::Delete(
    std::shared_ptr<t_destination> dest)
{
  if (!dest)
    return false;
  return Delete(dest->GetIdentHash());
}

template <typename t_destination>
bool LocalDestinations<t_destination>::Delete(const core::IdentHash& ident)
{
  if (ident.IsZero())
    return false;
  std::unique_lock<std::mutex> lock(m_Mutex);
  auto it = m_Destinations.find(ident);
  if (it != m_Destinations.end())
    {
      auto dest = it->second;
      {
        m_Destinations.erase(it);
        lock.unlock();
      }
      dest->Stop();
    }
  return true;
}

template <typename t_destination>
void LocalDestinations<t_destination>::Start()
{
  std::lock_guard<std::mutex> lock(m_Mutex);
  for (const auto& dest : m_Destinations)
    dest.second->Start();
}

template <typename t_destination>
void LocalDestinations<t_destination>::Stop()
{
  std::lock_guard<std::mutex> lock(m_Mutex);
  for (const auto& dest : m_Destinations)
    dest.second->Stop();
  m_Destinations.clear();
}

// Simply instantiating in namespace scope ties into, and is limited by, the current singleton design
// TODO(unassigned): refactoring this requires global work but will help to remove the singleton
ClientContext context;

ClientContext::ClientContext() : m_Exception(__func__) {}

// TODO(anonimal): nearly all Start/Stop handlers throughout the code-base should be replaced with proper RAII
void ClientContext::Start()
{
  // Initialize shared local dest here because of the context singleton
  m_SharedLocalDestination = m_LocalDestinations.Create();
  m_LocalDestinations.Start();
  LOG(debug) << "ClientContext: shared local destination started";

  m_HttpProxy->Start();
  LOG(debug) << "ClientContext: HTTP Proxy started";
  m_SocksProxy->Start();
  LOG(debug) << "ClientContext: SOCKS Proxy Started";
  // Start all client tunnels
  for (auto& pair : m_ClientTunnels)
    pair.second->Start();
  // Start all server tunnels
  for (auto& pair : m_ServerTunnels)
    pair.second->Start();
  // I2P Control
  if (m_I2PControlService) {
    LOG(debug) << "ClientContext: starting I2PControlService";
    m_I2PControlService->Start();
  }
  m_AddressBook.Start(m_SharedLocalDestination);
}

// TODO(anonimal): nearly all Start/Stop handlers throughout the code-base should be replaced with proper RAII
void ClientContext::Stop() {
  std::lock_guard<std::mutex> lockClient(m_ClientMutex);
  std::lock_guard<std::mutex> lockServer(m_ServerMutex);
  if (m_HttpProxy) {
    m_HttpProxy->Stop();
    m_HttpProxy.reset(nullptr);
    LOG(debug) << "ClientContext: HTTP Proxy stopped";
  }
  if (m_SocksProxy) {
    m_SocksProxy->Stop();
    m_SocksProxy.reset(nullptr);
    LOG(debug) << "ClientContext: SOCKS Proxy stopped";
  }
  for (auto& it : m_ClientTunnels) {
    it.second->Stop();
    LOG(debug)
      << "ClientContext: I2P client tunnel on port " << it.first << " stopped";
  }
  m_ClientTunnels.clear();
  for (auto& it : m_ServerTunnels) {
    it.second->Stop();
    LOG(debug) << "ClientContext: I2P server tunnel stopped";
  }
  m_ServerTunnels.clear();
  if (m_I2PControlService) {
    m_I2PControlService->Stop();
    m_I2PControlService.reset(nullptr);
    LOG(debug) << "ClientContext: I2PControl stopped";
  }
  m_AddressBook.Stop();
  m_SharedLocalDestination = nullptr;
  m_LocalDestinations.Stop();
  m_Service.stop();
}

void ClientContext::RequestShutdown() {
  Stop();
  if (m_ShutdownHandler)
    m_ShutdownHandler();
}

void ClientContext::RemoveTunnels(
    const std::vector<std::string>& client,
    const std::vector<std::string>& server)
{
  RemoveClientTunnels([&client](I2PClientTunnel* tunnel) {
    return std::find(
               client.begin(),
               client.end(),
               tunnel->GetTunnelAttributes().name)
           == client.end();
  });
  RemoveServerTunnels([&server](I2PServerTunnel* tunnel) {
    return std::find(
               server.begin(),
               server.end(),
               tunnel->GetTunnelAttributes().name)
           == server.end();
  });
}

void ClientContext::RemoveClientTunnels(
    std::function<bool(I2PClientTunnel*)> predicate) {
  std::lock_guard<std::mutex> lock(m_ClientMutex);
  for (auto it = m_ClientTunnels.begin(); it != m_ClientTunnels.end();) {
    if (predicate(it->second.get()))
      it = m_ClientTunnels.erase(it);
    else
      ++it;
  }
}

void ClientContext::RemoveServerTunnels(
    std::function<bool(I2PServerTunnel*)> predicate) {
  std::lock_guard<std::mutex> lock(m_ServerMutex);
  for (auto it = m_ServerTunnels.begin(); it != m_ServerTunnels.end();) {
    if (predicate(it->second.get()))
      it = m_ServerTunnels.erase(it);
    else
      ++it;
  }
}

void ClientContext::UpdateServerTunnel(
    const TunnelAttributes& tunnel,
    bool is_http) {
  bool create_tunnel = false;
  try {
    core::PrivateKeys keys;
    keys.LoadFromFile(tunnel.keys);
    const core::IdentHash& ident = keys.GetPublic().GetIdentHash();
    // Check if server already exists
    auto server_tunnel = GetServerTunnel(ident);
    if (server_tunnel == nullptr) {
      // Server with this name does not exist, create it later
      create_tunnel = true;
    } else {
      // Server exists, update tunnel attributes and re-bind tunnel
      // Note: all type checks, etc. are already completed in config handling
      server_tunnel->UpdateServerTunnel(tunnel);
      // TODO(unassigned): since we don't want to stop existing connections on
      // this tunnel we want to stay away from clearing any handlers
      // (e.g., not calling any stop function) but we need to ensure that
      // the previous bound port is also closed. Needs Review.
      // TODO(unassigned): consider alternative name (Apply instead of Start)
      m_ServerTunnels[ident]->Start();
    }
  } catch (const std::ios_base::failure&) {
    // Key file does not exist (assuming the tunnel is new)
    create_tunnel = true;
  } catch (const std::exception& ex) {
    throw std::runtime_error(
        "ClientContext: exception in " + std::string(__func__)
        + ": " + std::string(ex.what()));
  } catch (...) {
    throw std::runtime_error(
        "ClientContext: unknown exception in " + std::string(__func__));
  }
  if (create_tunnel)
    AddServerTunnel(tunnel, is_http);
}

void ClientContext::UpdateClientTunnel(
    const TunnelAttributes& tunnel) {
  auto client_tunnel = GetClientTunnel(tunnel.name);
  if (client_tunnel == nullptr) {
    // Client tunnel does not exist yet, create it
    AddClientTunnel(tunnel);
  } else {
    // Client with this name is already locally running, update settings
    std::string current_addr = client_tunnel->GetTunnelAttributes().address;
    boost::system::error_code ec;
    auto next_addr = boost::asio::ip::address::from_string(tunnel.address, ec);
    bool rebind = false;
    if (ec)  // New address is not an IP address, compare strings
      rebind = (tunnel.address != current_addr);
    else  // New address is an IP address, compare endpoints
      rebind =
          (client_tunnel->GetEndpoint()
           != boost::asio::ip::tcp::endpoint(next_addr, tunnel.port));
    // Note: We also use rebind even if local address/port hasn't changed
    // as a quick/easy way to drop previous connections
    if (!rebind)  // check for change in destination
      {
        LOG(debug)
            << "ClientContext: update checking for change in destination";
        rebind = (client_tunnel->GetTunnelAttributes().dest != tunnel.dest)
                 || (client_tunnel->GetTunnelAttributes().dest_port
                     != tunnel.dest_port);
      }
    if (!tunnel.keys.empty())  // check for change in keys
      {
        LOG(debug) << "ClientContext: update checking for change in keys";
        auto local_destination = m_LocalDestinations.LoadFromFile(tunnel.keys);
        if (local_destination->GetIdentHash()
            != client_tunnel->GetLocalDestination()->GetIdentHash())
          {
            client_tunnel->SetLocalDestination(local_destination);
            rebind = true;
          }
      }
    client_tunnel->SetTunnelAttributes(tunnel);
    if (rebind) {
      try {
        client_tunnel->Rebind(tunnel.address, tunnel.port);
      } catch (const std::exception& err) {
        LOG(error)
          << "ClientContext: failed to rebind "
          << tunnel.name << ": " << err.what();
      }
    }
  }
}

bool ClientContext::AddServerTunnel(
    const TunnelAttributes& tunnel,
    bool is_http)
{
  auto local_destination = m_LocalDestinations.LoadFromFile(tunnel.keys, true);
  auto server_tunnel =
      is_http ? std::make_unique<kovri::client::I2PServerTunnelHTTP>(
                    tunnel, local_destination)
              : std::make_unique<kovri::client::I2PServerTunnel>(
                    tunnel, local_destination);
  // Insert server tunnel
  if (!InsertServerTunnel(
          local_destination->GetIdentHash(), std::move(server_tunnel)))
    {
      LOG(error) << "Instance: server tunnel for destination "
                 << GetAddressBook().GetB32AddressFromIdentHash(
                        local_destination->GetIdentHash())
                 << " already exists";
      return false;
    }
  return true;
}

bool ClientContext::AddClientTunnel(const TunnelAttributes& tunnel)
{
  std::shared_ptr<kovri::client::ClientDestination> local_destination;
  if (!tunnel.keys.empty())
    local_destination = m_LocalDestinations.LoadFromFile(tunnel.keys);
  // Insert client tunnel
  auto client_tunnel = std::make_unique<kovri::client::I2PClientTunnel>(
      tunnel, local_destination);
  if (!InsertClientTunnel(tunnel.port, std::move(client_tunnel)))
    {
      LOG(error) << "Instance: failed to insert new client tunnel";
      return false;
    }
  return true;
}

void ClientContext::RegisterShutdownHandler(
    std::function<void(void)> handler) {
  m_ShutdownHandler = handler;
}

bool ClientContext::InsertClientTunnel(
    int port,
    std::unique_ptr<I2PClientTunnel> tunnel) {
  std::lock_guard<std::mutex> lock(m_ClientMutex);
  return m_ClientTunnels.insert(
      std::make_pair(port, std::move(tunnel))).second;
}

bool ClientContext::InsertServerTunnel(
    const kovri::core::IdentHash& id,
    std::unique_ptr<I2PServerTunnel> tunnel) {
  std::lock_guard<std::mutex> lock(m_ServerMutex);
  return m_ServerTunnels.insert(
      std::make_pair(id, std::move(tunnel))).second;
}

void ClientContext::SetI2PControlService(
    std::unique_ptr<kovri::client::I2PControlService> service) {
  m_I2PControlService = std::move(service);
}

void ClientContext::SetHTTPProxy(
    std::unique_ptr<HTTPProxy> proxy) {
  m_HttpProxy = std::move(proxy);
}

void ClientContext::SetSOCKSProxy(
    std::unique_ptr<kovri::client::SOCKSProxy> proxy) {
  m_SocksProxy = std::move(proxy);
}

I2PServerTunnel* ClientContext::GetServerTunnel(const std::string& name)
{
  std::lock_guard<std::mutex> lock(m_ServerMutex);
  auto it = std::find_if(
      m_ServerTunnels.begin(), m_ServerTunnels.end(),
      [&name](ServerTunnelEntry & e) -> bool {
        return e.second->GetName() == name;
      });
  return it == m_ServerTunnels.end() ? nullptr : it->second.get();
}

I2PServerTunnel* ClientContext::GetServerTunnel(
    const kovri::core::IdentHash& id)
{
  std::lock_guard<std::mutex> lock(m_ServerMutex);
  auto it = m_ServerTunnels.find(id);
  return it == m_ServerTunnels.end() ? nullptr : it->second.get();
}

I2PClientTunnel* ClientContext::GetClientTunnel(const std::string& name)
{
  std::lock_guard<std::mutex> lock(m_ClientMutex);
  auto it = std::find_if(
      m_ClientTunnels.begin(), m_ClientTunnels.end(),
      [&name](ClientTunnelEntry & e) -> bool {
        return e.second->GetName() == name;
      });
  return it == m_ClientTunnels.end() ? nullptr : it->second.get();
}

I2PClientTunnel* ClientContext::GetClientTunnel(int port)
{
  std::lock_guard<std::mutex> lock(m_ClientMutex);
  auto it = m_ClientTunnels.find(port);
  return it == m_ClientTunnels.end() ? nullptr : it->second.get();
}

boost::asio::io_service& ClientContext::GetIoService() {
  return m_Service;
}

}  // namespace client
}  // namespace kovri
