/**
 * Copyright (c) 2015-2018, The Kovri I2P Router Project
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be
 *    used to endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "util/i2pcontrol_client.h"

#include <iomanip>
#include <iostream>
#include <sstream>
#include <utility>

#include "core/util/byte_stream.h"

namespace asio = boost::asio;
namespace core = kovri::core;
namespace http = boost::beast::http;

namespace kovri
{
namespace client
{
I2PControlClient::I2PControlClient(std::shared_ptr<boost::asio::io_service> service)
    : m_Service(service),
      m_HTTPRequest(http::verb::post, "/", 11),
      m_Resolver(*service),
      m_Socket(*service)
{
}

void I2PControlClient::SetHost(const std::string& host)
{
  m_Host = host;
}

void I2PControlClient::SetPort(std::uint16_t port)
{
  m_Port = port;
}

void I2PControlClient::SetPassword(const std::string& password)
{
  m_Password = password;
}

void I2PControlClient::AsyncConnect(
    std::function<void(std::unique_ptr<Response>)> callback)
{
  auto request = std::make_shared<Request>();
  request->SetID(std::size_t(0));
  request->SetMethod(Method::Authenticate);
  request->SetParam(Request::MethodAuthenticate::API, std::size_t(1));
  request->SetParam(Request::MethodAuthenticate::Password, m_Password);
  m_Request.swap(request);

  ProcessAsyncSendRequest([this, callback](std::unique_ptr<Response> response) {
    if (response->GetError() == Response::ErrorCode::None)  // Store token
      {
        LOG(debug) << "I2PControlClient: Authentication successful";
        m_Token =
            response->GetParam<std::string>(Request::MethodAuthenticate::Token);
      }
    else
      {
        LOG(debug) << "I2PControlClient: Authentication failed!";
      }
    callback(std::move(response));
  });
}

void I2PControlClient::AsyncSendRequest(
    std::shared_ptr<I2PControlRequest> request,
    std::function<void(std::unique_ptr<Response>)> callback)
{
  m_Request.swap(request);

  // First try
  ProcessAsyncSendRequest(
      [this, callback](std::unique_ptr<Response> response) {
        // Received response
        switch (response->GetError())
          {
            case ErrorCode::NonexistentToken:
            case ErrorCode::ExpiredToken:
              // Auto re-authenticate
              AsyncConnect(
                  [this, callback](std::unique_ptr<Response>) {
                    // Try one last time
                    ProcessAsyncSendRequest(callback);
                  });
              break;
            default:
              callback(std::move(response));
              break;
          }
      });
}

void I2PControlClient::ProcessAsyncSendRequest(
    std::function<void(std::unique_ptr<Response>)> callback)
{
  m_Callback = callback;

  m_Resolver.async_resolve(
      m_Host,
      boost::lexical_cast<std::string>(m_Port),
      std::bind(
          &I2PControlClient::HandleAsyncResolve,
          this,
          std::placeholders::_1,
          std::placeholders::_2));
}

void I2PControlClient::HandleAsyncResolve(
    const boost::system::error_code& ec,
    const boost::asio::ip::tcp::resolver::results_type& results)
{
  if (ec)
    throw boost::system::system_error(ec);

  // Make the connection on the IP address we get from a lookup
  boost::asio::async_connect(
      m_Socket,
      results.begin(),
      results.end(),
      std::bind(
          &I2PControlClient::HandleAsyncConnect,
          this,
          std::placeholders::_1));
}

void I2PControlClient::HandleAsyncConnect(const boost::system::error_code& ec)
{
  if (ec)
    throw boost::system::system_error(ec);

  if (m_Request->GetMethod() != Method::Authenticate)
    m_Request->SetToken(m_Token);

  // Prepare the HTTP request
  m_HTTPRequest.set(http::field::host, m_Host);
  m_HTTPRequest.set(http::field::content_type, "application/json");
  m_HTTPRequest.body() = m_Request->ToJsonString();
  m_HTTPRequest.prepare_payload();

  // Write the request to the socket
  LOG(trace) << "I2PControlClient: sending " << m_HTTPRequest.body().data();
  http::async_write(
      m_Socket,
      m_HTTPRequest,
      std::bind(
          &I2PControlClient::HandleAsyncWrite,
          this,
          std::placeholders::_1,
          std::placeholders::_2));
}

void I2PControlClient::HandleAsyncWrite(
    const boost::system::error_code& ec,
    const std::size_t)
{
  if (ec)
    throw boost::system::system_error(ec);

  // Read the response from the socket
  http::async_read(
      m_Socket,
      m_Buffer,
      m_HTTPResponse,
      std::bind(
          &I2PControlClient::HandleHTTPResponse,
          this,
          std::placeholders::_1,
          std::placeholders::_2));
}

void I2PControlClient::HandleHTTPResponse(
    boost::system::error_code const& error,
    std::size_t const bytes_transferred)
{
  if (error && error != asio::error::eof)  // Connection closed cleanly by peer.
    throw boost::system::system_error(error);  // Some other error.

  boost::ignore_unused(bytes_transferred);

  std::stringstream stream;
#if (BOOST_VERSION >= 107000)
  stream << boost::beast::make_printable(m_HTTPResponse.body().data());
#else
  stream << boost::beast::buffers(m_HTTPResponse.body().data());
#endif

  // Clean up response before next request
  m_HTTPResponse.body().consume(m_HTTPResponse.body().size());

  LOG(trace) << "I2PControlClient: received " << stream.str();
  auto response = std::make_unique<Response>();
  response->Parse(m_Request->GetMethod(), stream);
  m_Callback(std::move(response));
}

}  // namespace client
}  // namespace kovri
