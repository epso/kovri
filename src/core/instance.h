/**                                                                                           //
 * Copyright (c) 2015-2018, The Kovri I2P Router Project                                      //
 *                                                                                            //
 * All rights reserved.                                                                       //
 *                                                                                            //
 * Redistribution and use in source and binary forms, with or without modification, are       //
 * permitted provided that the following conditions are met:                                  //
 *                                                                                            //
 * 1. Redistributions of source code must retain the above copyright notice, this list of     //
 *    conditions and the following disclaimer.                                                //
 *                                                                                            //
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list     //
 *    of conditions and the following disclaimer in the documentation and/or other            //
 *    materials provided with the distribution.                                               //
 *                                                                                            //
 * 3. Neither the name of the copyright holder nor the names of its contributors may be       //
 *    used to endorse or promote products derived from this software without specific         //
 *    prior written permission.                                                               //
 *                                                                                            //
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY        //
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF    //
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL     //
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       //
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,               //
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    //
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,          //
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF    //
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               //
 */

#ifndef SRC_CORE_INSTANCE_H_
#define SRC_CORE_INSTANCE_H_

#include <atomic>
#include <memory>
#include <string>
#include <vector>

#if defined(WITH_INTEGRATION_TESTS)
#include "core/router/net_db/impl.h"
#include "core/router/tunnel/impl.h"
#include "core/router/transports/impl.h"
#endif

namespace kovri
{
namespace core
{
// TODO(unassigned): properly remove the need to forward declare for external API usage
class Configuration;
class Exception;

/// @class Instance
/// @brief Core instance implementation
class Instance
{
 public:
  /// @param args argc + argv style options
  explicit Instance(
      const std::vector<std::string>& args = std::vector<std::string>());
  ~Instance() = default;

  Instance(const Instance&) = default;
  Instance& operator=(const Instance&) = default;

  Instance(Instance&&) = default;
  Instance& operator=(Instance&&) = default;

  /// @brief Initializes router context / core settings
  void Initialize();

  /// @brief Starts instance
  void Start();

  /// @brief Stops instance
  void Stop();

  /// @brief Get configuration object
  /// @return Reference to configuration object
  const Configuration& GetConfig() const noexcept
  {
    return *m_Config;
  }

#if defined(WITH_INTEGRATION_TESTS)
  bool is_running() const noexcept
  {
    return netdb.is_running() && tunnels.is_running() && transports.is_running();
  }
#endif

  struct Status
  {
    Status() = default;
    ~Status() = default;

    bool IsRunning() const;
    std::size_t GetActivePeersCount() const;
    std::size_t GetActiveRouterTunnelsCount() const;
    std::size_t GetFloodfillsCount() const;
    std::size_t GetKnownPeersCount() const;
    std::size_t GetLeaseSetsCount() const;
    std::size_t GetRouterTunnelsCreationRate() const;
    std::string GetDataPath() const;
    std::string GetState() const;
    std::string GetVersion() const;
    std::uint32_t GetInBandwidth() const;
    std::uint32_t GetOutBandwidth() const;
    std::uint64_t GetUptime() const;

    // TODO(unassigned): ib/ob tunnel lists
  };

  /// @brief Router status accessor
  const std::shared_ptr<Status>& GetStatus() const
  {
    return m_Status;
  };

 private:
  /// @brief Exception dispatcher
  static Exception m_Exception;

  /// @brief Client configuration implementation
  std::shared_ptr<Configuration> m_Config;

  /// @brief Router status facade
  std::shared_ptr<Status> m_Status;

  /// @brief Is Instance running?
  static std::atomic<bool> m_IsRunning;
};

}  // namespace core
}  // namespace kovri

#endif  // SRC_CORE_INSTANCE_H_
