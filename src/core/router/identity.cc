/**                                                                                           //
 * Copyright (c) 2013-2019, The Kovri I2P Router Project                                      //
 *                                                                                            //
 * All rights reserved.                                                                       //
 *                                                                                            //
 * Redistribution and use in source and binary forms, with or without modification, are       //
 * permitted provided that the following conditions are met:                                  //
 *                                                                                            //
 * 1. Redistributions of source code must retain the above copyright notice, this list of     //
 *    conditions and the following disclaimer.                                                //
 *                                                                                            //
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list     //
 *    of conditions and the following disclaimer in the documentation and/or other            //
 *    materials provided with the distribution.                                               //
 *                                                                                            //
 * 3. Neither the name of the copyright holder nor the names of its contributors may be       //
 *    used to endorse or promote products derived from this software without specific         //
 *    prior written permission.                                                               //
 *                                                                                            //
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY        //
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF    //
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL     //
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       //
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,               //
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    //
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,          //
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF    //
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               //
 *                                                                                            //
 * Parts of the project are originally copyright (c) 2013-2015 The PurpleI2P Project          //
 */

#include "core/router/identity.h"

#include <boost/endian/conversion.hpp>
#include <boost/date_time/gregorian/gregorian_types.hpp>

#include "core/crypto/hash.h"
#include "core/crypto/rand.h"
#include "core/crypto/signature.h"

#include "core/router/context.h"

#include "core/util/log.h"

namespace kovri {
namespace core {

// TODO(unassigned): identity implementation needs a big refactor

// TODO(unassigned): keep an eye open for alignment issues and for hacks like:
// copy public and signing keys together
//memcpy(public_key, keys.public_key, sizeof(public_key) + sizeof(signing_key));

Identity& Identity::operator=(const Keys& keys) {
  memcpy(public_key, keys.public_key, sizeof(public_key));
  memset(&certificate, 0, sizeof(certificate));
  return *this;
}

IdentHash Identity::Hash() const {
  IdentHash hash;
  // TODO(anonimal): this try block should be handled entirely by caller
  try {
    kovri::core::SHA256().CalculateDigest(
        hash, public_key, IdentitySizes::Default);
  } catch (...) {
    core::Exception ex;
    ex.Dispatch(__func__);
    // TODO(anonimal): review if we need to safely break control, ensure exception handling by callers
    throw;
  }
  return hash;
}

IdentityEx::IdentityEx()
    : m_StandardIdentity {},
      m_IdentHash {},
      m_Verifier(nullptr),
      m_ExtendedLen(0),
      m_ExtendedBuffer(nullptr),
      m_Exception(__func__) {}

IdentityEx::IdentityEx(
    const std::uint8_t* public_key,
    const std::uint8_t* signing_key,
    SigningKeyType type) {
  try
    {
      memcpy(
          m_StandardIdentity.public_key,
          public_key,
          sizeof(m_StandardIdentity.public_key));
      if (type == SigningKeyType::EdDsaSha512Ed25519)
        {
          constexpr std::size_t padding =
              crypto::PkLen::DSA - crypto::PkLen::Ed25519;  // 96 = 128 - 32
          kovri::core::RandBytes(m_StandardIdentity.signing_key, padding);
          memcpy(
              m_StandardIdentity.signing_key + padding,
              signing_key,
              crypto::PkLen::Ed25519);
          m_ExtendedLen = 4;  // 4 bytes extra
          // fill certificate
          m_StandardIdentity.certificate.type = CERTIFICATE_TYPE_KEY;
          m_StandardIdentity.certificate.length =
              boost::endian::native_to_big(m_ExtendedLen);
          // fill extended buffer
          m_ExtendedBuffer = std::make_unique<std::uint8_t[]>(m_ExtendedLen);
          core::OutputByteStream::Write<std::uint16_t>(
              m_ExtendedBuffer.get(), core::GetType(type));
          core::OutputByteStream::Write<std::uint16_t>(
              m_ExtendedBuffer.get() + 2,
              core::GetType(CryptoKeyType::ElGamal));
          // calculate ident hash
          auto buf = std::make_unique<std::uint8_t[]>(GetFullLen());
          ToBuffer(buf.get(), GetFullLen());
          kovri::core::SHA256().CalculateDigest(
              m_IdentHash, buf.get(), GetFullLen());
        }
      else if (type == SigningKeyType::DsaSha1)
        {
          memcpy(
              m_StandardIdentity.signing_key,
              signing_key,
              sizeof(m_StandardIdentity.signing_key));
          memset(
              &m_StandardIdentity.certificate,
              0,
              sizeof(m_StandardIdentity.certificate));
          m_IdentHash = m_StandardIdentity.Hash();
          m_ExtendedLen = 0;
          m_ExtendedBuffer.reset(nullptr);
        }
      else
        {
          LOG(warning) << "IdentityEx: signing key type "
                       << static_cast<int>(type) << " is not supported";
          throw std::invalid_argument(
              "IdentityEx: unsupported signing key type");
        }
      CreateVerifier();
  } catch (...) {
    m_Exception.Dispatch(__func__);
    // TODO(anonimal): review if we need to safely break control, ensure exception handling by callers
    throw;
  }
}

IdentityEx::IdentityEx(
    const std::uint8_t* buf,
    std::size_t len)
    : m_Verifier(nullptr),
      m_ExtendedLen(0),
      m_ExtendedBuffer(nullptr) {
  FromBuffer(buf, len);
}

IdentityEx::IdentityEx(
    const IdentityEx& other)
    : m_Verifier(nullptr),
      m_ExtendedBuffer(nullptr) {
  *this = other;
}

IdentityEx::~IdentityEx() {}

IdentityEx& IdentityEx::operator=(const IdentityEx& other) {
  if (&other != this) {
   memcpy(
       &m_StandardIdentity,
       &other.m_StandardIdentity,
       IdentitySizes::Default);
   m_IdentHash = other.m_IdentHash;
   m_ExtendedLen = other.m_ExtendedLen;
   m_ExtendedBuffer.reset(nullptr);
   if (m_ExtendedLen)
     {
       m_ExtendedBuffer = std::make_unique<std::uint8_t[]>(m_ExtendedLen);
       // Ensure that source buffer is not null before we copy (see #432)
       if (!other.m_ExtendedBuffer.get())
         throw std::runtime_error(
             "IdentityEx: other extended buffer is null");
       memcpy(
           m_ExtendedBuffer.get(),
           other.m_ExtendedBuffer.get(),
           m_ExtendedLen);
     }
   m_Verifier.reset(nullptr);
  }
  return *this;
}

IdentityEx& IdentityEx::operator=(const Identity& standard) {
  m_StandardIdentity = standard;
  m_IdentHash = m_StandardIdentity.Hash();
  m_ExtendedBuffer.reset(nullptr);
  m_ExtendedLen = 0;
  m_Verifier.reset(nullptr);
  return *this;
}

std::size_t IdentityEx::FromBuffer(
    const std::uint8_t* buf,
    std::size_t len) {
  if (len < IdentitySizes::Default) {
    LOG(error) << "IdentityEx: identity buffer length " << len << " is too small";
    return 0;
  }
  m_ExtendedLen = 0;
  m_ExtendedBuffer.reset(nullptr);
  memcpy(&m_StandardIdentity, buf, IdentitySizes::Default);
  if (m_StandardIdentity.certificate.length) {
    m_ExtendedLen = boost::endian::big_to_native(m_StandardIdentity.certificate.length);
    if (std::size_t(m_ExtendedLen + IdentitySizes::Default) <= len) {
      m_ExtendedBuffer = std::make_unique<std::uint8_t[]>(m_ExtendedLen);
      memcpy(
          m_ExtendedBuffer.get(), buf + IdentitySizes::Default, m_ExtendedLen);
    } else {
      LOG(error) << "IdentityEx: certificate length " << m_ExtendedLen
                 << " exceeds buffer length " << len - IdentitySizes::Default;
      return 0;
    }
  }
  // TODO(anonimal): this try block should be larger or handled entirely by caller
  try {
    kovri::core::SHA256().CalculateDigest(m_IdentHash, buf, GetFullLen());
  } catch (...) {
    m_Exception.Dispatch(__func__);
    // TODO(anonimal): review if we need to safely break control, ensure exception handling by callers
    throw;
  }
  m_Verifier.reset(nullptr);
  return GetFullLen();
}

std::size_t IdentityEx::ToBuffer(
    std::uint8_t* buf,
    std::size_t buflen) const {
  if (buflen < IdentitySizes::Default)
    throw std::length_error("IdentityEx: supplied buffer is too small");

  memcpy(buf, &m_StandardIdentity, IdentitySizes::Default);
  if (m_ExtendedLen > 0 && m_ExtendedBuffer)
  {
    if (buflen < std::size_t(IdentitySizes::Default + m_ExtendedLen))
      throw std::length_error("IdentityEx: supplied buffer is too small");

    memcpy(buf + IdentitySizes::Default, m_ExtendedBuffer.get(), m_ExtendedLen);
  }
  return GetFullLen();
}

void IdentityEx::FromBase32(const std::string& encoded)
{
  auto const decoded = core::Base32::Decode(encoded.c_str(), encoded.length());
  if (!FromBuffer(decoded.data(), decoded.size()))
    throw std::runtime_error("IdentityEx: could not decode from base32");
}

void IdentityEx::FromBase64(const std::string& encoded)
{
  auto const decoded = core::Base64::Decode(encoded.c_str(), encoded.length());
  if (!FromBuffer(
          decoded.data(),
          decoded.size()))
    throw std::runtime_error("IdentityEx: could not decode from base64");
}

std::string IdentityEx::ToBase32() const
{
  core::Buffer<IdentitySizes::Default, IdentitySizes::Extended> buf;
  buf(ToBuffer(buf.data(), buf.size()));
  return core::Base32::Encode(buf.data(), buf.size());
}

std::string IdentityEx::ToBase64() const
{
  std::array<std::uint8_t, IdentitySizes::Extended> buf{{}};
  std::size_t len = ToBuffer(buf.data(), buf.size());
  std::string const encoded(core::Base64::Encode(buf.data(), len));
  return encoded;
}

std::string GetCertificateTypeName(std::uint8_t type)
{
  switch (type)
    {
      case CERTIFICATE_TYPE_NULL:
        return "Null";
      case CERTIFICATE_TYPE_HASHCASH:
        return "Hashcash";
      case CERTIFICATE_TYPE_HIDDEN:
        return "Hidden";
      case CERTIFICATE_TYPE_SIGNED:
        return "Signed";
      case CERTIFICATE_TYPE_MULTIPLE:
        return "Multiple";
      case CERTIFICATE_TYPE_KEY:
        return "Key";
      default:
        LOG(error) << "Unknown type " << type;
        return "";
    }
}

std::string IdentityEx::GetDescription(const std::string& tabs) const
{
  std::stringstream ss;
  ss << tabs << "RouterIdentity: " << std::endl
     << tabs << "\tHash: " << GetIdentHash().ToBase64() << std::endl
     << tabs << "\tCertificate:" << std::endl
     << tabs << "\t\ttype: "
     << GetCertificateTypeName(m_StandardIdentity.certificate.type)
     << " certificate " << std::endl
     << tabs << "\t\tCrypto type: " << kovri::core::GetType(GetCryptoKeyType()) << std::endl
     << tabs << "\tPublickey: size: 256" << std::endl
     << tabs << "\tSigningPublickey: " << std::endl
     << tabs << "\t\t" << GetSigningKeyTypeName(GetSigningKeyType())
     << std::endl
     << tabs << "\t\tsize: " << GetSigningPublicKeyLen() << std::endl
     << tabs << "\t\t";
  if (GetSigningKeyType() == SigningKeyType::EdDsaSha512Ed25519)
    {
        ss << "Padding: " << (crypto::PkLen::DSA - crypto::PkLen::Ed25519);
    }
  else
    {
      LOG(warning) << "IdentityEx: signing key type "
                   << kovri::core::GetType(GetSigningKeyType())
                   << " is not supported";
    }
  ss << std::endl
     << tabs << "\tSignature: size: " << GetSignatureLen() << std::endl;
  return ss.str();
}

std::size_t IdentityEx::GetSigningPublicKeyLen() const {
  if (!m_Verifier)
    CreateVerifier();
  if (m_Verifier)
    return m_Verifier->GetPublicKeyLen();
  return crypto::PkLen::DSA;
}

std::size_t IdentityEx::GetSigningPrivateKeyLen() const {
  if (!m_Verifier)
    CreateVerifier();
  if (m_Verifier)
    return m_Verifier->GetPrivateKeyLen();
  return crypto::SkLen::DSA;
}

std::size_t IdentityEx::GetSignatureLen() const {
  if (!m_Verifier)
    CreateVerifier();
  if (m_Verifier)
    return m_Verifier->GetSignatureLen();
  return crypto::SigLen::DSA;
}

bool IdentityEx::Verify(
    const std::uint8_t* buf,
    std::size_t len,
    const std::uint8_t* signature) const {
  if (!m_Verifier)
    CreateVerifier();
  if (m_Verifier) {
    // TODO(anonimal): this try block should be handled entirely by caller
    try {
      return m_Verifier->Verify(buf, len, signature);
    } catch (...) {
      core::Exception ex;
      ex.Dispatch(__func__);
      // TODO(anonimal): review if we need to safely break control, ensure exception handling by callers
      throw;
    }
  }
  return false;
}

SigningKeyType IdentityEx::GetSigningKeyType() const {
  if (m_StandardIdentity.certificate.type ==
      CERTIFICATE_TYPE_KEY && m_ExtendedBuffer)
    {
      const auto signing_key_type = core::InputByteStream::Read<std::uint16_t>(
          m_ExtendedBuffer.get());  // signing key
      if (signing_key_type == core::GetType(SigningKeyType::EdDsaSha512Ed25519))
        return SigningKeyType::EdDsaSha512Ed25519;
      else
        {
          LOG(warning) << "IdentityEx: " << __func__
                       << ": unsupported signing key type";
          return SigningKeyType::Unsupported;
        }
    }
  return SigningKeyType::DsaSha1;
}

CryptoKeyType IdentityEx::GetCryptoKeyType() const {
  if (m_StandardIdentity.certificate.type ==
      CERTIFICATE_TYPE_KEY && m_ExtendedBuffer)
    {
      const std::uint16_t crypto_key_type =
          core::InputByteStream::Read<std::uint16_t>(
              m_ExtendedBuffer.get() + 2);  // crypto key
      if (crypto_key_type == kovri::core::GetType(CryptoKeyType::ElGamal))
        return CryptoKeyType::ElGamal;
      else
        {
          LOG(warning) << "IdentityEx: " << __func__
                       << ": unsupported crypto key type";
          return CryptoKeyType::Unsupported;
        }
    }
  return CryptoKeyType::ElGamal;
}

void IdentityEx::CreateVerifier() const  {
  const auto key_type = GetSigningKeyType();
  if (key_type == SigningKeyType::EdDsaSha512Ed25519)
    {
      std::size_t padding =
          crypto::PkLen::DSA - crypto::PkLen::Ed25519;  // 96 = 128 - 32
      m_Verifier = std::make_unique<kovri::core::Ed25519Verifier>(
          m_StandardIdentity.signing_key + padding);
    }
  else if(key_type == SigningKeyType::DsaSha1)
    {
      m_Verifier = std::make_unique<kovri::core::DSAVerifier>(
          m_StandardIdentity.signing_key);
    }
  else
    LOG(warning) << "IdentityEx: signing key type "
                 << kovri::core::GetType(key_type) << " is not supported";
}

void IdentityEx::DropVerifier() {
  m_Verifier.reset(nullptr);
}

/**
 *
 * PrivateKeys
 *
 */

PrivateKeys::PrivateKeys() : m_Signer(nullptr) {}
PrivateKeys::~PrivateKeys() {}

PrivateKeys& PrivateKeys::operator=(const Keys& keys) {
  m_Public = Identity(keys);
  memcpy(m_PrivateKey, keys.private_key, 256);  // 256
  memcpy(
      m_SigningPrivateKey,
      keys.signing_private_key,
      m_Public.GetSigningPrivateKeyLen());
  m_Signer.reset(nullptr);
  CreateSigner();
  return *this;
}

PrivateKeys& PrivateKeys::operator=(const PrivateKeys& other) {
  m_Public = other.m_Public;
  memcpy(m_PrivateKey, other.m_PrivateKey, 256);  // 256
  memcpy(
      m_SigningPrivateKey,
      other.m_SigningPrivateKey,
      m_Public.GetSigningPrivateKeyLen());
  m_Signer.reset(nullptr);
  CreateSigner();
  return *this;
}

std::size_t PrivateKeys::FromBuffer(
    const std::uint8_t* buf,
    std::size_t len) {
  std::size_t ret = m_Public.FromBuffer(buf, len);
  memcpy(m_PrivateKey, buf + ret, 256);  // private key always 256
  ret += 256;
  std::size_t signing_private_key_size = m_Public.GetSigningPrivateKeyLen();
  memcpy(m_SigningPrivateKey, buf + ret, signing_private_key_size);
  ret += signing_private_key_size;
  m_Signer.reset(nullptr);
  CreateSigner();
  return ret;
}

std::size_t PrivateKeys::ToBuffer(
    std::uint8_t* buf,
    std::size_t len) const {
  std::size_t ret = m_Public.ToBuffer(buf, len);
  memcpy(buf + ret, m_PrivateKey, 256);  // private key always 256
  ret += 256;
  std::size_t signing_private_key_size = m_Public.GetSigningPrivateKeyLen();
  memcpy(buf + ret, m_SigningPrivateKey, signing_private_key_size);
  ret += signing_private_key_size;
  return ret;
}

void PrivateKeys::Sign(
    const std::uint8_t* buf,
    int len,
    std::uint8_t* signature) const {
  if (m_Signer) {
    try {
      m_Signer->Sign(buf, len, signature);
    } catch (...) {
      core::Exception ex;
      ex.Dispatch(__func__);
      // TODO(anonimal): review if we need to safely break control, ensure exception handling by callers
      throw;
    }
  }
}

void PrivateKeys::CreateSigner() {
  if (m_Public.GetSigningKeyType() == SigningKeyType::EdDsaSha512Ed25519)
    m_Signer =
        std::make_unique<kovri::core::Ed25519Signer>(m_SigningPrivateKey);
  else
    LOG(warning) << "IdentityEx: Signing key type "
                 << kovri::core::GetType(m_Public.GetSigningKeyType())
                 << " is not supported";
}

PrivateKeys PrivateKeys::CreateRandomKeys(SigningKeyType type) {
  PrivateKeys keys;
  // TODO(anonimal): this try block should be handled entirely by caller
  try {
    // signature
    std::uint8_t signing_public_key[crypto::PkLen::Ed25519];  // signing public key is 32 bytes max
    if (type == SigningKeyType::EdDsaSha512Ed25519)
        kovri::core::CreateEd25519KeyPair(
            keys.m_SigningPrivateKey, signing_public_key);
    else
      {
        LOG(warning) << "IdentityEx: Signing key type "
                     << kovri::core::GetType(type)
                     << " is not supported, creating EDDSA-SHA512-ED25519";
        return PrivateKeys(
            kovri::core::CreateRandomKeys());  // EDDSA-SHA512-ED25519
      }
    // encryption
    std::uint8_t public_key[256];
    kovri::core::GenerateElGamalKeyPair(keys.m_PrivateKey, public_key);
    // identity
    keys.m_Public = IdentityEx(public_key, signing_public_key, type);
    keys.CreateSigner();
  } catch (...) {
    core::Exception ex;
    ex.Dispatch(__func__);
    // TODO(anonimal): review if we need to safely break control, ensure exception handling by callers
    throw;
  }
  return keys;
}

Keys CreateRandomKeys() {
  Keys keys;
  // TODO(anonimal): this try block should be handled entirely by caller
  try {
    // encryption
    kovri::core::GenerateElGamalKeyPair(
        keys.private_key,
        keys.public_key);
    // signing
    kovri::core::CreateEd25519KeyPair(
        keys.signing_private_key,
        keys.signing_key);
  } catch (...) {
    core::Exception ex;
    ex.Dispatch(__func__);
    // TODO(anonimal): review if we need to safely break control, ensure exception handling by callers
    throw;
  }
  return keys;
}

void PrivateKeys::LoadFromFile(const std::string& filename)
{
  try
    {
      // TODO(unassigned): util/filesystem
      const auto file_path =
          (core::GetPath(core::Path::ClientKeys) / filename).string();
      std::ifstream file(file_path, std::ifstream::binary);
      if (!file)
        {
          LOG(debug) << "PrivateKeys: " << file_path
                     << " does not exist, creating";
          *this = CreateFile(filename);
          return;
        }

      file.seekg(0, std::ios::end);
      const std::size_t len = file.tellg();
      file.seekg(0, std::ios::beg);
      std::unique_ptr<std::uint8_t[]> buf(
          std::make_unique<std::uint8_t[]>(len));
      file.read(reinterpret_cast<char*>(buf.get()), len);

      PrivateKeys keys;
      keys.FromBuffer(buf.get(), len);
      // Contingency: create associated address text file if the private keys
      // filename is swapped out with another set of keys with the same filename
      CreateDestTextFiles(keys, filename);
      LOG(info) << "PrivateKeys: " << file_path
                << " loaded: uses local address "
                << core::GetB32Address(keys.GetPublic().GetIdentHash());
      *this = keys;
    }
  catch (...)
    {
      core::Exception ex("PrivateKeys");
      ex.Dispatch(__func__);
      throw;
    }
}

PrivateKeys PrivateKeys::CreateFile(const std::string& filename) const
{
  const auto path = core::EnsurePath(core::GetPath(core::Path::ClientKeys));
  const auto file_path = (path / filename).string();

  // TODO(unassigned): util/filesystem
  // Create binary keys file
  std::ofstream file(file_path, std::ofstream::binary);

  if (!file)
    throw std::runtime_error("PrivateKeys: could not open file for writing");

  const auto& keys = CreateRandomKeys();  // Generate default type
  std::size_t len = keys.GetFullLen();
  std::unique_ptr<std::uint8_t[]> buf(std::make_unique<std::uint8_t[]>(len));
  len = keys.ToBuffer(buf.get(), len);
  file.write(reinterpret_cast<char*>(buf.get()), len);

  // Create associated address text file
  CreateDestTextFiles(keys, filename);
  LOG(info) << "PrivateKeys: created new keypair " << file_path;

  return keys;
}

void PrivateKeys::CreateDestTextFiles(
    const kovri::core::PrivateKeys& keys,
    const std::string& filename) const
{
  LOG(debug) << "PrivateKeys: creating destination text files";

  // Get the identity to encode
  const core::IdentityEx& ident = keys.GetPublic();

  // Ensure real-time path to write to
  const std::string file =
      (core::EnsurePath(core::GetPath(core::Path::ClientKeys)) / filename)
          .string();

  // Create base32 address file
  LOG(info) << "PrivateKeys: saving base32 address to " << file << ".b32.txt";
  std::ofstream b32(file + ".b32.txt");
  if (!b32)
    throw std::runtime_error("couldn't open b32 text file for writing");
  std::string address = core::GetB32Address(ident.GetIdentHash());
  b32 << address;
  LOG(info) << "PrivateKeys: this destination's base32 address is: " << address;

  // Create base64 address file
  LOG(info) << "PrivateKeys: saving base64 address to " << file << ".b64.txt";
  std::ofstream b64(file + ".b64.txt");
  if (!b64)
    throw std::runtime_error("couldn't open b64 text file for writing");
  address = ident.ToBase64();
  b64 << address;
  LOG(info) << "PrivateKeys: this destination's base64 address is: " << address;
}

// TODO(unassigned): should not be a free function
IdentHash CreateRoutingKey(const IdentHash& ident)
{
  IdentHash key;
  try
    {
      if (ident.IsZero())
        throw std::invalid_argument("Identity: zero-initialized ident");

      // Get spec-defined formatted date
      std::string const date(GetFormattedDate());

      // Create buffer for key
      std::vector<std::uint8_t> buf(
          ident(), ident() + (ident.size() + date.size()));

      // Append the ISO date
      buf.insert(buf.end(), date.begin(), date.end());

      // Hash the key
      assert(key.size() <= buf.size());
      kovri::core::SHA256().CalculateDigest(key(), buf.data(), buf.size());
    }
  catch (...)
    {
      kovri::core::Exception ex(__func__);
      ex.Dispatch();
      throw;
    }
  return key;
}

// TODO(unassigned): should not be a free function
std::string GetFormattedDate()
{
  boost::gregorian::date date(boost::gregorian::day_clock::universal_day());
  return boost::gregorian::to_iso_string(date);
}

XORMetric operator^(
    const IdentHash& key1,
    const IdentHash& key2) {
  XORMetric m;
  const std::uint64_t* hash1 = key1.GetLL(), * hash2 = key2.GetLL();
  m.metric_ll[0] = hash1[0] ^ hash2[0];
  m.metric_ll[1] = hash1[1] ^ hash2[1];
  m.metric_ll[2] = hash1[2] ^ hash2[2];
  m.metric_ll[3] = hash1[3] ^ hash2[3];
  return m;
}

const std::string GetSigningKeyTypeName(const SigningKeyType key)
{
  if (key == SigningKeyType::EdDsaSha512Ed25519)
    return "EDDSA_SHA512_ED25519";
  else if (key == SigningKeyType::DsaSha1)
    return "DSA_SHA1";
  else
    {
      LOG(error) << "Unknown signing key type " << kovri::core::GetType(key);
      return "";
    }
}

}  // namespace core
}  // namespace kovri
